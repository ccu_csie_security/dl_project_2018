Our source code, models, dataset and reference material can be found in the uploaded manuscript of the report (ReportFile_DL_Project_2018.pdf) or please check here

1. https://drive.google.com/drive/u/0/folders/1KDDgdKLvYnw1hTf8b25xJacecj-ZR_KZ 

2. https://bitbucket.org/ccu_csie_security/dl_project_2018

Thank you for reviewing our project.