\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}

\begin{document}

\title{A mobile-friendly deep learning approach for obstacles detection}

\author{\IEEEauthorblockN{Van-Linh Nguyen 805410003, Manh-Hung Ha 805415007, Cheng-Wei Chiang 806450001, \\ Lan-Huong Nguyen 806410006, Ching Hung 403410039, Duy-Thanh Tran 605415155}
	\IEEEauthorblockA{Department of Computer Science and Information Engineering\\
		National Chung Cheng University, Chiayi, Taiwan 62102\\
		Corresponding author email: nvlinh@cs.ccu.edu.tw}}
\maketitle


\begin{abstract}
 The deep learning continues to gain influence in many aspects of our life such as autonomous driving and natural language processing. However, the memory-consuming tasks of deep learning require the powerful computing servers which is beyond the reach of end-users. Recent days, an emerging trend is to develop the learning system which can easily deploy on mobile or constrained devices. Therefore, in this work, we present a practical approach to build a mobile-friendly detection system, so-called MobileDeep, which can flexibly execute the lightweight deep learning architecture to detect obstacles on the road such as traffic lights and signs. Based on SSD Mobilenet V2 and self-collected dataset, our detector can detect about 70\% defined obstacles on the front of driving car in real-time (up to 30 frames per second). Besides the demonstration of the method in autonomous driving, we believe that our approach may also inspire the research community in bring the features of deep learning to the other applications and thus improve the experience of users.
\end{abstract}

\begin{IEEEkeywords}
 Mobile-friendly Deep learning, Obstacle Detection, Autonomous Driving
\end{IEEEkeywords}

\section{Introduction}
\label{chap:intro}

Recent years, the deep learning trending promises to bring a lot of benefits which consists of increased safety in autonomous driving, smart traffic management and so on. However, the model in their proposals requires a lot memory and resources to train/operate \cite{Fridman17}. From design perspective, the faster system runs, the safer it is due to the importance of the immediate responses on the real-time events. A deep learning system can be embedded in constrained devices promises a huge potential deployment, particularly, on billions of mobile users. 

There are several efforts \cite{Kim16, Huang17, Howard17, Sandler18} to develop a mobile-friendly deep learning system. However, their models have not yet been deployed on specific applications. Therefore, in this work, we present a practical approach to build a mobile-friendly detection system, so-called MobileDeep, which can flexibly run a lightweight deep learning architecture to detect obstacles on the road such as traffic lights and signs. Based on SSD Mobilenet V2 \cite{Mobilenetv2} and self-collected dataset, our detector can detect up to 70\% defined obstacles on the front of driving car in real-time. To improve the detection accuracy, we combine variety of tunings to obtain and perceive the surrounding environmental information such as spot center image and fixed lane of road. As illustrated in Fig.~\ref{fig:obstacle}, the obstacles consist of traffic lights, signs, pedestrians or cars ahead in the detection region of the camera. Note that the diversity of the obstacles on the road has created big challenges to autonomous driving and detection technologies.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/obstacle}
	\end{center}
	\caption{The obstacles on the road}
	\label{fig:obstacle}
\end{figure}

The article makes the following contributions: 

\begin{itemize} 
	
	\item Build a mobile-friendly obstacle detection system which can detect major obstacles on the front of a driving car in real-time.
	\item Detail how to build a lightweight self-collected dataset for training/testing in a specific application.  
	\item Our open approach may inspire the community to build useful applications on mobile devices.
\end{itemize}

The remainder of this paper is organized as follows. The overview of obstacle detection in autonomous driving and mobile-friendly deep learning approach is presented in Section~\ref{chap:background}. Section~\ref{chap:proposal} and Section~\ref{chap:evaluation} present our proposal and evaluations, respectively. Finally, the conclusion of our paper is presented in Section~\ref{chap:conclusion}.

\section{Background and related work}
\label{chap:background}

\subsection{Obstacle detection and Autonomous Driving}
\label{chap:autonomousdriving}

Self-driving or the autonomous driving of a vehicle is to define a specific target in real traffic without the intervention of a human driver. Such a vehicle gets its input data primarily from visual information sources that are also available to the driver. In the preliminary stages of autonomous driving, technology enhanced a driver’s awareness by providing information that enabled him or her to make a decision and react quickly \cite{Fridman17}. Therefore, autonomous cars combine a variety of techniques to perceive their surroundings, including radar, laser light, GPS, odometry and computer vision. Advanced control systems interpret sensory information to identify appropriate navigation paths, as well as obstacles and relevant signs \cite{Huang17}. Other obstacles could be missing driver experience in potentially dangerous situations, ethical problems in situations where an autonomous car's software is forced during an unavoidable crash to choose between multiple harmful courses of action and possibly insufficient Adaptation to Gestures and non-verbal cues by police and pedestrians \cite{Huang17}. In this application, we do not emphasize to using advanced technologies such as laser and GPS the but visual warn about potentially dangerous obstacles to the driver based on a real-time detection camera on the front of the car. 

\subsection{Mobile-friendly Deep learning}
\label{chap:mobiledeeplearning}

In the past few years, convolutional neural networks (CNN) have risen to popularity due to their effectiveness in various computer vision tasks. Networks like AlexNet, VGGNet, and GoogleNet have been proved to be very effective for image classification. However, deep networks are very computationally intensive and thus they require multiple high-end GPUs in order to function in a real world scenario. Running them on a CPU takes multiple seconds, even much longer on a mobile phone \cite{Sandler18}. Also, their models are huge in size. Loading a large model causes delay and consumes high amount of power as it would have to be stored in the DRAM.  But all hope is not lost. Many researchers \cite{Howard17,Sandler18, Mobilenetv2,Chollet17} are focusing on this very problem. There are two ways to make deep CNNs faster:
\begin{enumerate}
  \item Make the network shallow.
  \item Improve the underlying implementation of the convolutional layers.
\end{enumerate}

The first way comes straight by training a given dataset using an ensemble of deep nets and then transferring the function learned on these deep nets into a shallow net. However, simply training the shallow net using the original data gives an accuracy which is significantly lower \cite{Huang17}. Thus, by transferring the function we can make the shallow network learn a much more complex function which otherwise would not have been possible by training directly on the dataset. 

The second method is the favorite of major researchers. In such that, they use tensor rank decomposition techniques to decrease the number of operations that need to be done for each layer. The authors \cite{Kim16} use Variational Bayesian Matrix Factorization for rank selection and tucker-2-decomposition to split each layer into three layers. In two recent years, few researchers starts working on a mobile-friendly approach such as \cite{Liu16, Howard17, Huang17, Sandler18}. 

In this work, we inherit the model architecture of the state of art in mobile-friendly deep learning approach \cite{Sandler18} but apply for a specific application (obstacle detection)and updated on our self-collected dataset.


\section{The proposed system}
\label{chap:proposal}
In this section, we present the system architecture, the self-collected dataset, implementation of the proposal. 

\subsection{Architecture}
\label{chap:architecture}

The architecture, namely MobileDeep, involves two parts: (1) Detection Engine (2) Visual Display.

The first engine consists the core of Mobilenet V2 \cite{Sandler18} learning algorithm but we tune the hyper parameters on the constrained resources of our mobile devices. Also, by leveraging APIs from the latest TensorFlow Object Detection API, our footsteps obtain the significant benefits towards a near-practical deployment. Through training, we highlight the limitation of existing datasets \cite{Bosch,GTSDB} and thus propose to collect a new dataset for local traffic - in Taiwan. 

The second engine is the input data capturing and the output visual display. To offer a real-time monitoring feature, we use OpenCV 3.4.1 to capture video stream ( or alternative option is Web Real-Time Communications - WebRTC). The library works well with sources are camera/webcam/game screening. We also use MoviePy library to generate the output video with acceptable quality. 


\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/architecture}
	\end{center}
	\caption{The proposal architecture}
	\label{fig:architecture}
\end{figure}

For the detection region, we use a central point of camera to align and set a fixed distance (pixels) to its two sides. The lane size of the road is also set in advance.

\subsection{Self-collected dataset}
\label{self-collected-dataset}

 For the evaluation purpose, the dataset used in this work was taken from three sources: Bosch, GTSDB, and our own dataset. The first one consists of 5093 images of traffic lights in Germany for training and 8334 images for testing while the second includes over 39000 images of traffic signs in Germany for training and over 12000 images for testing. To adopt the characteristics of local traffic, we have collected 300+ images of the road condition in Taiwan for training and 58 images for testing. Regarding the labels of the data, the Bosch dataset includes 15 different traffic light labels for training but only 4 labels (red, yellow, green, off) for testing and the GTSDB includes more than 40 traffic sign labels and the images contain one traffic sign each. Moreover, images from GTSDB have the worst resolution (32x32 pixels). The images in our own dataset were captured by our cameras, and classified into 21 different labels of some common traffic signs and lights. We assume that all images in our dataset is under same distribution.
 
 For storage, labels in the Bosch dataset are saved at \textit{yaml} files. Class (red, green,..) and its id (1, 2..) will be store at a pbtxt file while GTSDB uses CSV file to map its images. Therefore, we must standardize or convert this raw data to proper format for processing. In addition, in the dataset, it contains some wrong labels, for example, the labels position stays in outside of the images. For this reason, we have to check data and labels before converting. An important step in building the self-collected dataset is the labeling. We brief the step-by-step this process as follows:
 
 \textbf{Labeling an image}
 
 For labeling the images to the given classes(e.g., red, green, green right), it is critical to identify the defined object correctly. In our case, we label different labels on an image as many as possible such as obstacle ahead, traffic lights and signs to reduce the need of a bigger dataset. We use software LabelImg \cite{labelimg} to label those images into custom-defined classes. The labeling process and how to choose the object shown in Fig.~\ref{fig:labelling}. We should take carefully to draw the bounding box around the object which we need to define. All the objects such as traffic lights and signs at the front of camera and in the detection region ( pixel calculation from the center of image) are called the obstacles. Labeling is one of the most time-consuming task, particularly, the multiple-objects identification on the single image. 
 
 \begin{figure}[ht]
 	\begin{center}
 		\includegraphics[width=1\linewidth]{related/labelling}
 	\end{center}
 	\caption{Labelling the self-collected images}
 	\label{fig:labelling}
 \end{figure}
 
 \textbf{Convert to the tensorflow record file}
 
 After labeling the images, several XML files are generated to represent the path of labeled image and coordination of each marked object. However, to let the tensorflow understand the new data, we need to convert the raw dataset to all-in-one tensorflow format (.record). All of these stages can be seen as a illustration in Fig.~\ref{fig:generate_dataset}. Note that we must do on both training data (train.record) and testing data (test.record). Also, the size of these converted record files must be similar to the size of all original training/testing images.
 
\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/generate_dataset}
	\end{center}
	\caption{Generate to the tensorflow record file}
	\label{fig:generate_dataset}
\end{figure}


\section{Evaluation}
\label{chap:evaluation}

We build the system based on TensorFlow Object Detection API \cite{tensorflowapi} and train the model with 4 datasets: Coco pre-trained (general), Bosch (traffic light), GTSDB (traffic sign), and our own dataset ( sample Taiwan traffic light + sign). The test file is the real-time video which were recorded in Minxiong, Chiayi, Taiwan. 

There are also many ways to train the model, run object detector inference and evaluation measure computations on given datasets such as using tools from Google Cloud Platform, however, in this project, we have done all tasks on our own server (Xenon E7, 128 GB RAM, the graphic card Nvidia K80). In the training, we use 20k training steps, IOU threshold: [0.6, 0.99], data augmentation: random horizontal flip. 
 
For the detection evaluation, we have found that the model can detect any object of 90 classes in Microsoft Coco dataset, as shown in Fig.~\ref{fig:before1} but they are general classes(is it car?). However, we have expected to classify the objects into specific categories such as red traffic light and max speed limitation. The existing datasets such as Coco are therefore  not suitable for this requirement but Bosch and GTSDB. Unluckily, the model after training with the last two datasets still faces the overfitting issue. As the predictable results, a high probability of wrong detection found, as illustrated in Fig.~\ref{fig:before2}. 


\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/before1}
	\end{center}
	\caption{The model with Coco pre-trained dataset}
	\label{fig:before1}
\end{figure}


\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/before2}
	\end{center}
	\caption{The model with GTSDB dataset}
	\label{fig:before2}
\end{figure}

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/bosch}
	\end{center}
	\caption{The model with Bosch dataset}
	\label{fig:bosch}
\end{figure}

Due to the local knowledge about dataset, the accuracy is fluctuated, e.g., testing with Bosch dataset in Fig.~\ref{fig:bosch}. The system can detect the red light but mismatched with the motorbike tail light. Note that MobileNet V2 is based on the Convolutional Neural Networks, while this model's features are more generic in early layers and more original-dataset-specific in later layers. To increase the accuracy in detecting the local traffic ( with a slight different distribution in collection), we use transfer-learning to re-train the model on our dataset with fine-tuning of the last checkpoint of the pre-trained coco models. This transfer technique  helps us to reduce the remarkable time. According to the time estimation, the approach is effective because the images were trained on a large corpus of objects (Coco Dataset). Fine-tuning from the pre-trained model also helps to optimize the the hyper parameters than organize input data, output data or the mapping from input to output. In our model, we can inherit the knowledge of car and sign detection to dig into more specific classes such as right/left traffic light/ max limit speed. 

In second scenario, we have tested our model with self-collected dataset. The model detects successfully new classes such as Obstacle\_ahead and GreenRight which are not in any existing datasets on the Internet but ours. The illustration is shown in Fig.~\ref{fig:result1} and Fig.~\ref{fig:result2}. To increase the accuracy of detection tasks, we have collected the images in training data with different angles and under morning/afternoon/dark/sunny environment conditions.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/result1}
	\end{center}
	\caption{The model with our self-collected dataset}
	\label{fig:result1}
\end{figure}


\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/result2}
	\end{center}
	\caption{The model with our self-collected dataset}
	\label{fig:result2}
\end{figure}

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\linewidth]{related/carprice}
	\end{center}
	\caption{The model with our self-collected dataset to suggest the price of old cars}
	\label{fig:carprice}
\end{figure}

In the final scenario, we have evaluated the models on real-time video (screening the Window Media player which is playing the recorded traffic video). We have found that the model can run smoothly with 30 frames per second and the detection accuracy is only about 70\%. In some situations such as the flashing (blink) traffic light or images reflecting on the mirror of the car, the model suggested the object incorrectly.


\subsection{Challenges and future work}
\label{chap:applicationtoreal} 

We have identifies several challenges after conducting this research. First, the method detects several objects incorrectly such as flashing light of restaurants in night time or wrong warning from traffic signs. Second, the model does not also work well on the special weather/surrounding environment condition such as rain or too dark. We believe that a bigger/diverse dataset for training may help to reduce these false detection situation.

Besides solving the addressed challenges, we have seen a lot of potential opportunities to apply the deep learning object detection techniques on mobile applications. For example, as illustrated in Fig.~\ref{fig:carprice}, the model can help to recognize the car and suggest useful information such as estimation price and age when the users capture that car by smart phone. In other case, the model can predict a disease if combine the results of skin analysis, blood and other health-care metrics from sensors. Also, another practical potential application is for augment virtual where we can capture the current furniture in the house and suggest the arrangement/changes. 

\section{Conclusion}
\label{chap:conclusion}

In this paper, we have presented a comprehensive way to build a a practical mobile-friendly obstacle detection system, namely MobileDeep. The system includes a core of SSD Mobilenet V2 and a self-collect dataset for Taiwan local traffic. We have evaluated the proposal in two scenarios, and the results show that our architecture can exactly detect simple obstacles such as traffic light and signs, although a false detection may be found in special condition when human may also be confused such as rain or too dark. We further note that the dataset building may play a critical role in improving the accuracy of the model. The pre-processing images should be done in advance to keep the distribution consistently. Finally, among the booming development of mobile augment applications, leveraging our approach, the developers can entirely build a rich feature detection application or integrate to a popular app.

All source code of our project can be found at \cite{sourcecode}.


\begin{thebibliography}{1}

	
	
	\bibitem{Fridman17}
	Lex Fridman et al, ``MIT Autonomous Vehicle Technology Study: Large-Scale Deep Learning Based Analysis of Driver Behavior and Interaction with Automation,'' https://arxiv.org/pdf/1711.06976.pdf, 2017
	
	\bibitem{Kim16}
	Yong-Deok Kim, Eunhyeok Park, Sungjoo Yoo, Taelim Choi, Lu Yang, Dongjun Shin, ``Compression of Deep Convolutional Neural Networks for Fast and Low Power Mobile Applications,'' https://arxiv.org/abs/1511.06530, 2016.
	
   \bibitem{Huang17}
	Jonathan Huang et al, ``Speed/accuracy trade-offs for modern convolutional object detectors,'' Google Research, Apr. 2017.
	
	\bibitem{Howard17}
	Andrew G. Howard, Menglong Zhu, Bo Chen, Dmitry Kalenichenko, Weijun Wang, Tobias Weyand, Marco Andreetto, and Hartwig Adam, ``Mobilenets: Efficient convolutional neural networks for mobile vision applications,'' CoRR, abs/1704.04861, 2017.
	
	
	\bibitem{Sandler18}
	Mark Sandler, Andrew Howard, Menglong Zhu, Andrey Zhmoginov, and Liang-Chieh Chen, ``MobileNetV2: Inverted Residuals and Linear Bottleneck,'' Google Inc, Apr. 2018.
	
    \bibitem{Mobilenetv2}
	Mobilenetv2 source code. Available from
	https://github.com/tensorflow/models/tree/master/research/slim/nets/mobilenet
	   
	
   \bibitem{Chollet17}
	 Francois Chollet, ``Xception: Deep learning with depthwise separable convolutions,'' IEEE Conference on Computer Vision and Pattern Recognition (CVPR), Jul. 2017.
	
   \bibitem{Liu16}
	Wei Liu, Dragomir Anguelov, Dumitru Erhan, Christian Szegedy, Scott Reed, Cheng-Yang Fu, and Alexander C Berg, ``Ssd: Single shot multibox detector,'' ECCV, 2016.
	
	\bibitem{opencv}
	https://opencv.org/
	
	\bibitem{webrtc}
	https://webrtc.org/
	
	\bibitem{moviepy}
	https://github.com/Zulko/moviepy
	
	\bibitem{labelimg}
	https://github.com/tzutalin/labelImg
	
	\bibitem{tensorflowapi}
	Jonathan Huang, Vivek Rathod, Derek Chow, Chen Sun, and Menglong Zhu. Tensorflow Object Detection API, 2017
	https://github.com/tensorflow/models/tree/master/research/object\_detection
	
    \bibitem{Bosch}
	Bosch Dataset
	https://hci.iwr.uni-heidelberg.de/node/6132
	
	\bibitem{GTSDB}
 	GTSDB Dataset
	http://benchmark.ini.rub.de/?section=gtsdb\&subsection=dataset
	
	\bibitem{sourcecode}
	https://bitbucket.org/ccu\_csie\_security/dl\_project\_2018
	
	

\end{thebibliography}

	\begin{table}[]
	\centering
	\caption{Team Collaboration}
	\label{teamcollaboration}
	\begin{tabular}{|l|l|}
		\hline
		\multicolumn{2}{|l|}{\begin{tabular}[c]{@{}l@{}}All members do together\\ - Identify problem statement, discuss the idea and confirm the goal\\ - Validate the final results\end{tabular}}                               \\ \hline
		Van-Linh Nguyen  & \begin{tabular}[c]{@{}l@{}}* Team Leader \\ - Propose the idea and assign tasks to members\\ - Combine the final model and evaluate the result\\ - Draft and revise the completed report\end{tabular} \\ \hline
		Manh-Hung Ha     & \begin{tabular}[c]{@{}l@{}} - Lane detection training \\ - Export the model   \end{tabular}                                                                                                                                                                         \\ \hline
		Cheng-Wei Chiang & \begin{tabular}[c]{@{}l@{}}- Traffic sign detection training\\ - Compare the accuracy of detection on the \\ local traffic sign and GTSDB dataset\end{tabular}                                        \\ \hline
		Lan-Huong Nguyen & \begin{tabular}[c]{@{}l@{}}- Road surface detection ( pothole)\\ - Collect relevant images\end{tabular}                                                                                               \\ \hline
		Chinh Hung       & \begin{tabular}[c]{@{}l@{}}- Collect images and build dataset for \\ Taiwan traffic light/sign\\ - Write the report about building dataset\end{tabular}                                                  \\ \hline
		Duy-Thanh Tran   & \begin{tabular}[c]{@{}l@{}}- Traffic light detection training\\ - Compare the accuracy of detection on the  \\ local traffic light and Bosch dataset\end{tabular}                                     \\ \hline
	\end{tabular}
\end{table}

\end{document}
